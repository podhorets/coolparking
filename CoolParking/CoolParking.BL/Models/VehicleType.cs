﻿namespace CoolParking.BL.Models
{
    public enum VehicleType
    {
        None,
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}