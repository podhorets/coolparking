﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; set; }
        public string VehicleId { get; set; }
        public DateTime TransactionDate { get; set; }

        public TransactionInfo(decimal sum, string vehicleId, DateTime transactionDate)
        {
            Sum = sum;
            VehicleId = vehicleId;
            TransactionDate = transactionDate;
        }
    }
}