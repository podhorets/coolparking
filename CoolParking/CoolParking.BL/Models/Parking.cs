﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal Balance { get; set; } = 0;
        public List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();

        private static Parking instance;

        private Parking()
        { }

        public static Parking GetInstance()
        {
            if (instance == null)
                instance = new Parking();
            return instance;
        }

        public void Dispose()
        {
            instance = null;
        }
    }
}