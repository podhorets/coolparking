﻿namespace CoolParking.BL.Models
{
    public class Messages
    {
        public static string WeclcomeText { get; } = "Hi! Here is Cool Parking.\n";
        public static string ActionEntryText { get; } = "\nWrite a number of the action which you need:\n\n";
        public static string ParkingActions { get; } =
            "0 --- Exit program.\n" +
            "1 --- Get parking balance.\n" +
            "2 --- Get parking capacity.\n" +
            "3 --- Get free places in parking.\n" +
            "4 --- Get all vehicles in parking.\n" +
            "5 --- Get vehicle by id.\n" +
            "6 --- Create vehicle.\n" +
            "7 --- Remove vehicle\n" +
            "8 --- Get last transactions.\n" +
            "9 --- Get transactions history.\n" +
            "10 --- Top up vehicle.\n";
        public static string VehicleTypeText { get; } = "\nPlease entry number of folowing vehicle types:\n" +
            "1 --- Passenger car\n" +
            "2 --- Truck\n" +
            "3 --- Bus\n" +
            "4 --- Motorcycle\n";
        public static string VehicleIdText { get; } = "\nWrite a number (id) of vehicle. (example: AA-1234-BB)";
        public static string VehicleIdRemoveText { get; } = "\nWrite a number (id) of vehicle for remove. (example: AA-1234-BB)";
        public static string VehicleIdTopUpText { get; } = "\nWrite a number (id) of vehicle for top up balance. (example: AA-1234-BB)";
        public static string VehicleBalanceText { get; } = "\nWrite a vehicle balance (just number up to 0).";
        public static string TopUpBalanceText { get; } = "\nWrite a balance for top up (just number up to 0).";
        public static string ErrorText { get; } = "\nWrong entry!";
        public static string ContinueText { get; } = "\nPress Enter to continue";
    }
}
