﻿namespace CoolParking.BL.Models
{
    public class Links
    {
        public static string Vehicles = "https://localhost:44306/api/vehicles/";
        public static string Parking = "https://localhost:44306/api/parking";
        public static string ParkingBalance = "https://localhost:44306/api/parking/balance";
        public static string ParkingCapacity = "https://localhost:44306/api/parking/capacity";
        public static string ParkingFreePlaces = "https://localhost:44306/api/parking/freePlaces";
        public static string LastTransactions = "https://localhost:44306/api/transactions/last";
        public static string TransactionsHistory = "https://localhost:44306/api/transactions/all";
        public static string TopUpVehicle = "https://localhost:44306/api/transactions/topUpVehicle";
    }
}
