﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Models
{
    public enum BalanceCondition
    {
        None,
        Positive,
        NegativeAfterPayment,
        Negative
    }
}
