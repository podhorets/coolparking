﻿namespace CoolParking.BL.Models
{
    public class Settings
    {
        public static double PaymentPeriod { get; } = 5000;
        public static double LoggingPeriod { get; } = 60000;
        public static int ParkingCapacity { get; } = 10;
        public static decimal PassengerCarTariff { get; } = 2;
        public static decimal TruckTariff { get; } = 5;
        public static decimal BusTariff { get; } = 3.5M;
        public static decimal MotorcycleTariff { get; } = 1;
        public static decimal PenaltyRate { get; } = 2.5M;
    }
}