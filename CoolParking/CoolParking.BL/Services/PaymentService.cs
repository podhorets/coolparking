﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoolParking.BL.Services
{
    public class PaymentService
    {
        public List<TransactionInfo> Transactions { get; set; } = new List<TransactionInfo>();

        public void DoPaymentForAllVehiclesInParking()
        {
            Parking.GetInstance().Vehicles.ForEach(vehicleItem => this.DoPaymentForSpecificCar(vehicleItem));
        }

        private void DoPaymentForSpecificCar(Vehicle vehicle)
        {
            BalanceCondition balanceCondition = this.GetBalanceCondition(vehicle);

            switch (balanceCondition)
            {
                case BalanceCondition.Positive:
                    this.DoStandardPayment(vehicle);
                    break;
                case BalanceCondition.NegativeAfterPayment:
                    this.DoPaymentWithPartialPenalty(vehicle);
                    break;
                case BalanceCondition.Negative:
                    this.DoPaymentWithPenalty(vehicle);
                    break;
                default:
                    // do nothing
                    break;
            }
        }

        /// <summary>
        /// If balance of car positive, only  standard tariff should be payed.
        /// Example: 
        /// Car Balance: 6
        /// Tariff: 2
        /// Car Balance after payment: 4
        /// </summary>
        private void DoStandardPayment(Vehicle vehicle)
        {
            var payment = vehicle.GetTariffByVehicleType();

            this.WithdrawPaymentFromVehicleBalance(vehicle, payment);
            this.SendPaymentToParkingBalance(payment);
            this.CreateTransactionForPayment(vehicle, payment);
        }

        /// <summary>
        /// If balance of car negative, tariff should be multiplied on penalty rate.
        /// Example: 
        /// Car Balance: -10
        /// Tariff: 2
        /// Penalty rate: 2.5
        /// Car Balance after payment: -15
        /// </summary>
        private void DoPaymentWithPenalty(Vehicle vehicle)
        {
            var payment = Settings.PenaltyRate * vehicle.GetTariffByVehicleType();

            this.WithdrawPaymentFromVehicleBalance(vehicle, payment);
            this.SendPaymentToParkingBalance(payment);
            this.CreateTransactionForPayment(vehicle, payment);
        }

        /// <summary>
        /// If balance of car positive, but after payment is negative, part of negative tariff should be multiplied on penalty rate.
        /// Example: 
        /// Car Balance: 1
        /// Tariff: 5
        /// Penalty rate: 2.5
        /// Car Balance after payment: -10 (negative part 4 * penalty rate 2.5)
        /// </summary>
        private void DoPaymentWithPartialPenalty(Vehicle vehicle)
        {
            var initialBalance = vehicle.Balance;

            var balanceAfterPayment = (initialBalance - vehicle.GetTariffByVehicleType()) * Settings.PenaltyRate;

            var payment = initialBalance - balanceAfterPayment;

            this.WithdrawPaymentFromVehicleBalance(vehicle, payment);
            this.SendPaymentToParkingBalance(payment);
            this.CreateTransactionForPayment(vehicle, payment);
        }

        private void WithdrawPaymentFromVehicleBalance(Vehicle vehicle, decimal payment)
        {
            vehicle.Balance -= payment;
        }

        private void SendPaymentToParkingBalance(decimal payment)
        {
            Parking.GetInstance().Balance += payment;
        }

        private void CreateTransactionForPayment(Vehicle vehicle, decimal payment)
        {
            TransactionInfo transaction = new TransactionInfo(payment, vehicle.Id, DateTime.Now);
            Transactions.Add(transaction);
        }

        private BalanceCondition GetBalanceCondition(Vehicle vehicle)
        {
            decimal tariff = vehicle.GetTariffByVehicleType();

            if (vehicle.Balance >= tariff)
            {
                return BalanceCondition.Positive;
            }
            else if (vehicle.Balance < tariff && vehicle.Balance >= 0)
            {
                return BalanceCondition.NegativeAfterPayment;
            }
            else if (vehicle.Balance < tariff)
            {
                return BalanceCondition.Negative;
            }

            return BalanceCondition.None;
        }
    }
}
