﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        private readonly TextService textBuilder;

        public LogService(string logFilePath)
        {
            LogPath = logFilePath;

            textBuilder = new TextService();
        }

        public string Read()
        {
            string[] lines = File.ReadAllLines(LogPath);
            string result = string.Join("\n", lines);

            return result;
        }

        public void Write(string logInfo)
        {
            this.CheckLogPath();

            if (string.IsNullOrEmpty(logInfo))
            {
                throw new ArgumentException(nameof(logInfo));
            }

            File.AppendAllText(LogPath, logInfo + "\n");
        }

        private void CheckLogPath()
        {
            if (string.IsNullOrEmpty(LogPath))
            {
                throw new ArgumentException(nameof(LogPath));
            }
        }
    }
}