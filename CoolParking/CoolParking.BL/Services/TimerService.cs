﻿using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer tp { get; }
        public double Interval { get; set; } = 0;

        public event ElapsedEventHandler Elapsed;

        public TimerService()
        {
            tp = new Timer();
        }

        public void Dispose()
        {
            tp.Dispose();
            tp.Enabled = false;
            tp.Elapsed -= Elapsed;
            Interval = 0;
            Elapsed = null;
        }

        public void Start()
        {
            if (Interval <= 0)
            {
                throw new ArgumentException(nameof(Interval));
            }

            tp.Interval = Interval;
            tp.Elapsed += Elapsed;
            tp.Enabled = true;
            tp.Start();
        }

        public void Stop()
        {
            tp.Stop();
            tp.Enabled = false;
            tp.Elapsed -= Elapsed;
        }
    }
}