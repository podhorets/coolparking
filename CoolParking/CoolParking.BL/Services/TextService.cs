﻿using CoolParking.BL.Models;
using System.Text;

namespace CoolParking.BL.Services
{
    public class TextService
    {
        public string CreateTransactionInfoText(TransactionInfo transaction)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append($"Transaction date: {transaction.TransactionDate}, ");
            builder.Append($"Vehicle Id: {transaction.VehicleId}, ");
            builder.Append($"Payment amount: {transaction.Sum}.");

            return builder.ToString();
        }
    }
}
