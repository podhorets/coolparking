﻿using CoolParking.BL.Dto;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Services
{
    public class ConsoleService
    {
        private static HttpClient client = new HttpClient();

        public static async Task ExecuteCalledAction(string enteredStringValue)
        {
            int.TryParse(enteredStringValue, out int enteredValue);

            switch (enteredValue)
            {
                case 0:
                    Environment.Exit(0);
                    break;
                case 1:
                    string balance = await GetParkingBalance();
                    Console.WriteLine(balance);
                    break;
                case 2:
                    string capacity = await GetParkingCapacity();
                    Console.WriteLine(capacity);
                    break;
                case 3:
                    string freePlaces = await GetParkingFreePlaces();
                    Console.WriteLine(freePlaces);
                    break;
                case 4:
                    string vehicles = await GetVehicles();
                    Console.WriteLine(vehicles);
                    break;
                case 5:
                    string vehicle = await GetVehicleById();
                    Console.WriteLine(vehicle);
                    break;
                case 6:
                    string createdVehicle = await CreateVehicle();
                    Console.WriteLine(createdVehicle);
                    break;
                case 7:
                    string result = await RemoveVehicle();
                    Console.WriteLine(result);
                    break;
                case 8:
                    string lastTransactions = await GetLastTransactions();
                    Console.WriteLine(lastTransactions);
                    break;
                case 9:
                    string transactionsHistory = await GetTransactionsHistory();
                    Console.WriteLine(transactionsHistory);
                    break;
                case 10:
                    string topUpVehicle = await TopUpVehicle();
                    Console.WriteLine(topUpVehicle);
                    break;
                default:
                    Console.WriteLine(Messages.ErrorText);
                    break;
            }
        }

        private static async Task<string> GetParkingBalance()
        {
            HttpResponseMessage response = await client.GetAsync(Links.ParkingBalance);
            if (response.IsSuccessStatusCode)
            {
                string balance = await response.Content.ReadAsStringAsync();
                return "Parking balance is: " + balance;
            }
            else
            {
                string errorMessage = await response.Content.ReadAsStringAsync();

                return errorMessage;
            }
        }

        private static async Task<string> GetParkingCapacity()
        {
            HttpResponseMessage response = await client.GetAsync(Links.ParkingCapacity);
            if (response.IsSuccessStatusCode)
            {
                string balance = await response.Content.ReadAsStringAsync();
                return "Parking capacity is: " + balance;
            }
            else
            {
                string errorMessage = await response.Content.ReadAsStringAsync();

                return errorMessage;
            }
        }

        private static async Task<string> GetParkingFreePlaces()
        {
            HttpResponseMessage response = await client.GetAsync(Links.ParkingFreePlaces);
            if (response.IsSuccessStatusCode)
            {
                string freePlaces = await response.Content.ReadAsStringAsync();
                return "Free places in parking: " + freePlaces;
            }
            else
            {
                string errorMessage = await response.Content.ReadAsStringAsync();

                return errorMessage;
            }
        }

        private static async Task<string> GetVehicles()
        {
            HttpResponseMessage response = await client.GetAsync(Links.Vehicles);
            if (response.IsSuccessStatusCode)
            {
                string vehiclesJson = await response.Content.ReadAsStringAsync();
                List<VehicleConsole> vehicles = JsonConvert.DeserializeObject<IEnumerable<VehicleConsole>>(vehiclesJson).ToList();

                StringBuilder builder = new StringBuilder();
                builder.Append($"At the moment in Cool Parking are {vehicles.Count} vehicles.\n");

                vehicles.ForEach(vehicle =>
                {
                    builder.Append($"Number of vehicle: {vehicle.Id}, type of vehicle: {vehicle.VehicleType}, balance of vehicle: {vehicle.Balance}.\n");
                });

                return builder.ToString();
            }
            else
            {
                string errorMessage = await response.Content.ReadAsStringAsync();

                return errorMessage;
            }
        }

        private static async Task<string> GetVehicleById()
        {
            Console.WriteLine(Messages.VehicleIdText);
            string vehicleId = Console.ReadLine();
            HttpResponseMessage response = await client.GetAsync(Links.Vehicles + vehicleId);

            if (response.IsSuccessStatusCode)
            {
                string vehiclesJson = await response.Content.ReadAsStringAsync();
                VehicleConsole vehicle = JsonConvert.DeserializeObject<VehicleConsole>(vehiclesJson);

                return $"Number of vehicle: {vehicle.Id}, type of vehicle: {vehicle.VehicleType}, balance of vehicle: {vehicle.Balance}.\n";
            }
            else
            {
                string errorMessage = await response.Content.ReadAsStringAsync();

                return errorMessage;
            }
        }

        private static async Task<string> CreateVehicle()
        {
            Console.WriteLine(Messages.VehicleIdText);
            string vehicleId = Console.ReadLine();

            Console.WriteLine(Messages.VehicleTypeText);
            string vehicleTypeString = Console.ReadLine();

            Console.WriteLine(Messages.VehicleBalanceText);
            string balanceString = Console.ReadLine();

            VehicleType vehicleType = GetVehicleType(vehicleTypeString);
            var balanceValid = decimal.TryParse(balanceString, out decimal balance);

            VehicleConsole newVehicle = new VehicleConsole{
                Id = vehicleId,
                VehicleType = vehicleType,
                Balance = balanceValid ? balance : 0
            };

            string newVehicleJson = JsonConvert.SerializeObject(newVehicle);

            HttpResponseMessage response = await client.PostAsJsonAsync(Links.Vehicles, newVehicleJson);
            if (response.IsSuccessStatusCode)
            {
                string vehiclesJson = await response.Content.ReadAsStringAsync();
                VehicleConsole vehicle = JsonConvert.DeserializeObject<VehicleConsole>(vehiclesJson);

                return $"\nVehicle created!\nNumber of vehicle: {vehicle.Id}, type of vehicle: {vehicle.VehicleType}, balance of vehicle: {vehicle.Balance}.\n";
            }
            else
            {
                string errorMessage = await response.Content.ReadAsStringAsync();

                return errorMessage;
            }
        }

        private static async Task<string> RemoveVehicle()
        {
            Console.WriteLine(Messages.VehicleIdText);
            string vehicleId = Console.ReadLine();

            HttpResponseMessage response = await client.DeleteAsync(Links.Vehicles + vehicleId);
            if (response.IsSuccessStatusCode)
            {
                return $"\nVehicle deleted!";
            }
            else
            {
                string errorMessage = await response.Content.ReadAsStringAsync();

                return errorMessage;
            }
        }

        private static async Task<string> GetLastTransactions()
        {
            HttpResponseMessage response = await client.GetAsync(Links.LastTransactions);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                string errorMessage = await response.Content.ReadAsStringAsync();

                return errorMessage;
            }
        }

        private static async Task<string> GetTransactionsHistory()
        {
            HttpResponseMessage response = await client.GetAsync(Links.TransactionsHistory);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                string errorMessage = await response.Content.ReadAsStringAsync();

                return errorMessage;
            }
        }

        private static async Task<string> TopUpVehicle()
        {
            Console.WriteLine(Messages.VehicleIdText);
            string vehicleId = Console.ReadLine();

            Console.WriteLine(Messages.TopUpBalanceText);
            string enteredSum = Console.ReadLine();
            var sumValid = decimal.TryParse(enteredSum, out decimal sum);

            var topUpVehicleDto = new TopUpVehicleDto
            {
                id = vehicleId,
                Sum = sumValid ? sum : 0
            };

            var topUpVehicleJson = JsonConvert.SerializeObject(topUpVehicleDto);
            var requestContent = new StringContent(topUpVehicleJson, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PutAsync(Links.TopUpVehicle, requestContent);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                string errorMessage = await response.Content.ReadAsStringAsync();

                return errorMessage;
            }
        }

        private static VehicleType GetVehicleType(string enteredVehicleTypeString)
        {
            VehicleType vehicleType;

            int.TryParse(enteredVehicleTypeString, out int enteredVehicleType);

            switch (enteredVehicleType)
            {
                case 1:
                    vehicleType = VehicleType.PassengerCar;
                    break;
                case 2:
                    vehicleType = VehicleType.Truck;
                    break;
                case 3:
                    vehicleType = VehicleType.Bus;
                    break;
                case 4:
                    vehicleType = VehicleType.Motorcycle;
                    break;
                default:
                    vehicleType = VehicleType.None;
                    break;
            }

            return vehicleType;
        }
    }

    public class VehicleConsole {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }
    }
}
