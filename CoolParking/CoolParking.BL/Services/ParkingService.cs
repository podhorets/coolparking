﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Validators;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private bool disposedValue;
        private readonly ITimerService paymentTimer;
        private readonly ITimerService loggerTimer;
        private readonly ILogService logger;

        private readonly VehicleValidator vehicleValidator;
        private readonly PaymentService paymentCalculation;
        private readonly TextService textBuilder;

        public ParkingService(ITimerService paymentTimer, ITimerService loggerTimer, ILogService logger)
        {
            this.paymentTimer = paymentTimer;
            this.loggerTimer = loggerTimer;
            this.logger = logger;

            vehicleValidator = new VehicleValidator();
            paymentCalculation = new PaymentService();
            textBuilder = new TextService();

            InitializePaymentIntervalCalculations(this.paymentTimer);
            InitializeLogIntervalHistory(this.loggerTimer, this.paymentTimer);
            StartTimers(new List<ITimerService> { this.loggerTimer, this.paymentTimer });
        }

        private void InitializePaymentIntervalCalculations(ITimerService paymentIntervalTimer)
        {
            paymentIntervalTimer.Elapsed += new ElapsedEventHandler((object sender, ElapsedEventArgs e) =>
            {
                paymentCalculation.DoPaymentForAllVehiclesInParking();
            });
        }

        private void InitializeLogIntervalHistory(ITimerService logIntervalTimer, ITimerService paymentIntervalTimer)
        {
            logIntervalTimer.Elapsed += new ElapsedEventHandler((object sender, ElapsedEventArgs e) =>
            {
                paymentIntervalTimer.Stop();

                foreach (var transaction in paymentCalculation.Transactions.ToList())
                {
                    string transactionInfoText = this.textBuilder.CreateTransactionInfoText(transaction);

                    this.logger.Write(transactionInfoText);
                }

                if (!paymentCalculation.Transactions.Any()) this.logger.Write($"No transactions for {DateTime.Now}");

                paymentCalculation.Transactions.Clear();

                paymentIntervalTimer.Start();
            });
        }

        private static void StartTimers(List<ITimerService> timers)
        {
            timers.ForEach((timerItem) => timerItem.Start());
        }

        public void AddVehicle(Vehicle vehicle)
        {
            Parking.GetInstance().Vehicles.Add(vehicle);
        }

        public decimal GetBalance()
        {
            return Parking.GetInstance().Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return Settings.ParkingCapacity - Parking.GetInstance().Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return this.paymentCalculation.Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.GetInstance().Vehicles);
        }

        public string ReadFromLog()
        {
            return this.logger.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            ValidateVehicleId(vehicleId);
            ValidateIfVehicleExists(vehicleId);

            Vehicle vehicle = this.GetVehicleById(vehicleId);
            Parking.GetInstance().Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            ValidateVehicleId(vehicleId);
            ValidateIfVehicleExists(vehicleId);

            bool isTopUpEntryNotValid = this.vehicleValidator.IsTopUpEntryValid(sum);
            if (isTopUpEntryNotValid)
            {
                throw new ArgumentException("Top up for vehicle can be only positive.");
            }

            this.GetVehicleById(vehicleId).Balance += sum;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Parking.GetInstance().Dispose();
                    this.loggerTimer.Dispose();
                    this.paymentTimer.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            System.GC.SuppressFinalize(this);
        }

        private Vehicle GetVehicleById(string vehicleId)
        {
            return Parking.GetInstance().Vehicles.FirstOrDefault(vehicleItem => vehicleItem.Id == vehicleId);
        }

        private void ValidateVehicleId(string vehicleId)
        {
            bool isIdValid = this.vehicleValidator.IsIdValid(vehicleId);

            if (!isIdValid)
            {
                throw new ArgumentException("Vehicle Id does not match the rule (Rule: AA-0000-AA).");
            }
        }

        private void ValidateIfVehicleExists(string vehicleId)
        {
            bool vehicleExists = this.vehicleValidator.IsVehicleExists(vehicleId);

            if (!vehicleExists)
            {
                throw new ArgumentException("Vehicle with Id: " + vehicleId + " is not exists.");
            }
        }
    }
}