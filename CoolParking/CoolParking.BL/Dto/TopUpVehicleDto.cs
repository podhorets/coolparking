﻿namespace CoolParking.BL.Dto
{
    public class TopUpVehicleDto
    {
        public string id { get; set; }
        public decimal Sum { get; set; }
    }
}
