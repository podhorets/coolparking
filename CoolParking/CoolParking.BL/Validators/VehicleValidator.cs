﻿using CoolParking.BL.Models;
using System.Linq;

namespace CoolParking.BL.Validators
{
    public class VehicleValidator
    {
        private readonly RegularExpressionValidator regex;

        public VehicleValidator()
        {
            regex = new RegularExpressionValidator();
        }

        public bool IsIdValid(string vehicleId)
        {
            var validVehicleIdPattern = regex.GetValidVehicleIdPattern();

            return validVehicleIdPattern.IsMatch(vehicleId);
        }

        public bool IsTopUpEntryValid(decimal sum)
        {
            return sum < 0;
        }

        public bool IsVehicleExists(string vehicleId)
        {
            return Parking.GetInstance().Vehicles.Any(vehicle => vehicle.Id == vehicleId);
        }

        public Vehicle GetVehicleById(string id)
        {
            return Parking.GetInstance().Vehicles.FirstOrDefault(vehicleItem => vehicleItem.Id == id);
        }
    }
}
