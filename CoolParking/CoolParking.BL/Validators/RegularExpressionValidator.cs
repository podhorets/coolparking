﻿using System.Text.RegularExpressions;

namespace CoolParking.BL.Validators
{
    public class RegularExpressionValidator
    {
        public Regex GetValidActionEntryPattern()
        {
            var validEntryPattern = new Regex("[0-9]");

            return validEntryPattern;
        }

        public Regex GetValidVehicleIdPattern()
        {
            var validVehicleIdPattern = new Regex("([A-Z])+(-)+([0-9])+(-)+([A-Z])\\w");

            return validVehicleIdPattern;
        }

        public Regex GetValidVehicleTypePattern()
        {
            var validVehicleTypePattern = new Regex("[1-4]");

            return validVehicleTypePattern;
        }
    }
}
