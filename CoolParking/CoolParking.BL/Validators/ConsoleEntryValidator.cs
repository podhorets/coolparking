﻿namespace CoolParking.BL.Validators
{
    public class ConsoleEntryValidator
    {
        private readonly RegularExpressionValidator regex;

        public ConsoleEntryValidator()
        {
            regex = new RegularExpressionValidator();
        }

        public bool IsActionEntryValid(string actionEntry)
        {
            var validVehicleIdPattern = regex.GetValidActionEntryPattern();

            return validVehicleIdPattern.IsMatch(actionEntry);
        }

        public bool IsVehicleTypeValid(string vehicleType)
        {
            var validVehicleTypePattern = regex.GetValidVehicleTypePattern();

            return validVehicleTypePattern.IsMatch(vehicleType);
        }
    }
}
