﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Validators;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService parkingService;
        private readonly VehicleValidator vehicleValidator;
        public VehiclesController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
            this.vehicleValidator = new VehicleValidator();
        }

        [HttpGet]
        public async Task<ActionResult<string>> GetVehicles()
        {
            var vehicles = this.parkingService.GetVehicles().ToArray();
            var vehiclesJson = JsonConvert.SerializeObject(vehicles);

            return Ok(vehiclesJson);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Vehicle>> GetById(string id)
        {
            bool isIdValid = this.vehicleValidator.IsIdValid(id);
            if (!isIdValid)
            {
                return BadRequest($"Vehicle with id {id} was not found");
            }

            var vehicle = this.vehicleValidator.GetVehicleById(id);
            if (vehicle == null)
            {
                return NotFound($"Vehicle with id {id} was not found");
            }

            return Ok(vehicle);
        }

        [HttpPost]
        public async Task<ActionResult<string>> Post([FromBody] string value)
        {
            Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(value);

            bool isIdValid = this.vehicleValidator.IsIdValid(vehicle.Id);
            bool balanceInValid = vehicleValidator.IsTopUpEntryValid(vehicle.Balance);

            if (!isIdValid || balanceInValid)
            {
                return BadRequest("Vehicle data is invalid! Write valide Id, VehicleType and Balance!");
            }

            bool vehicleExists = vehicleValidator.IsVehicleExists(vehicle.Id);

            if (vehicleExists)
            {
                return BadRequest("Vehicle is already created!");
            }

            this.parkingService.AddVehicle(vehicle);
            return Created(Links.Vehicles + vehicle.Id, vehicle);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<string>> Delete(string id)
        {
            bool isIdValid = this.vehicleValidator.IsIdValid(id);
            if (!isIdValid)
            {
                return BadRequest("Vehicle data is invalid! Write valide Id, VehicleType and Balance!");
            }

            Vehicle vehicle = this.vehicleValidator.GetVehicleById(id);
            if(vehicle == null)
            {
                return NotFound($"Vehicle with {id} was not found!");
            }

            this.parkingService.RemoveVehicle(id);
            return NoContent();
        }
    }
}
