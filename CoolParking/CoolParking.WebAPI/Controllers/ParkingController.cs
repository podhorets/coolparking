﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService parkingService;
        public ParkingController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        // GET api/<ParkingController>/balance
        [HttpGet]
        [Route("balance")]
        public async Task<ActionResult<decimal>> GetBalance()
        {
            return Ok(Parking.GetInstance().Balance);
        }

        // GET api/<ParkingController>/capacity
        [HttpGet]
        [Route("capacity")]
        public async Task<ActionResult<int>> GetCapacity()
        {
            return Ok(this.parkingService.GetCapacity());
        }

        // GET api/<ParkingController>/freePlaces
        [HttpGet]
        [Route("freePlaces")]
        public async Task<ActionResult<int>> GetFreePlaces()
        {
            return Ok(this.parkingService.GetFreePlaces());
        }
    }
}
