﻿using CoolParking.BL.Dto;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Validators;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService parkingService;
        private readonly ILogService logger;
        private readonly VehicleValidator vehicleValidator;

        public TransactionsController(IParkingService parkingService, ILogService logger)
        {
            this.parkingService = parkingService;
            this.logger = logger;
            this.vehicleValidator = new VehicleValidator();
        }

        // GET: api/<TransactionsController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<TransactionsController>/last
        [HttpGet]
        [Route("last")]
        public async Task<ActionResult<TransactionInfo[]>> GetLastParkingTransaction()
        {
            return Ok(this.parkingService.GetLastParkingTransactions());
        }

        // GET api/<TransactionsController>/all
        [HttpGet]
        [Route("all")]
        public async Task<ActionResult<TransactionInfo[]>> GetParkingHistory()
        {
            if (string.IsNullOrEmpty(this.logger.LogPath))
            {
                return NotFound("Log was not found!");
            }

            return Ok(this.logger.Read());
        }

        // PUT api/<TransactionsController>/topUpVehicle
        [HttpPut]
        [Route("topUpVehicle")]
        public async Task<ActionResult<TransactionInfo[]>> TopUpVehicleBalance([FromBody]TopUpVehicleDto topUpVehicleDto)
        {
            //TopUpVehicleDto topUpVehicleDto = JsonConvert.DeserializeObject<TopUpVehicleDto>(value);
            string id = topUpVehicleDto.id;
            decimal sum = topUpVehicleDto.Sum;

            bool isIdValid = this.vehicleValidator.IsIdValid(id);
            bool balanceInValid = vehicleValidator.IsTopUpEntryValid(sum);

            if (!isIdValid || balanceInValid)
            {
                return BadRequest("Vehicle data is invalid! Write valide Id, VehicleType and Balance!");
            }

            Vehicle vehicle = this.vehicleValidator.GetVehicleById(id);
            if (vehicle == null)
            {
                return NotFound($"Vehicle with {id} was not found!");
            }

            this.parkingService.TopUpVehicle(id, sum);

            return Ok(vehicle);
        }
    }
}
