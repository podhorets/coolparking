﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking
{
    class Program
    {
        static async Task Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine(Messages.ActionEntryText + Messages.ParkingActions);
                var entry = Console.ReadLine();
                var validEntryPattern = new Regex("[0-9]");
                bool isEntryValid = validEntryPattern.IsMatch(entry);

                if (isEntryValid)
                {
                    await ConsoleService.ExecuteCalledAction(entry);
                }
                else
                {
                    Console.WriteLine("Wrong entry. Entry should be a number from 0 to 9");
                }

                Console.WriteLine("Press Enter to continue");
                Console.ReadKey();
            }
        }
    }
}
